#include "EmberEngine.h"

Ember::Engine* pEngine = 0;

void Render()
{
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, PSTR szCmdLine, int iCmdShow)
{
	pEngine = Create(123);

	if (pEngine) pEngine->Run(L"Ember Engine Test", hInstance, Render);

	pEngine->Render();

	pEngine->Close();
	return 0;
}