#pragma once

#include "EmberStableHeaders.h"

namespace Ember
{
	class Engine
	{
	public:
		virtual bool CALL Run(LPCWSTR sTitle, HINSTANCE hInstance, void (*pRenderProc)(), HWND hWndParent = 0, int nWidth = 800, int nHeight = 600, bool bWindowed = true) = 0;
		virtual void CALL Render() = 0;
		virtual	void CALL Close() = 0;

		virtual void CALL AddLable(int nID, LPCWSTR sText, LPCWSTR sName, _VECTOR2 mPos = _VECTOR2(), _COLOR mColor = _COLOR()) = 0;
		virtual void CALL DeleteGUI(int nID) = 0;
	};
}

extern "C" { EXPORT Ember::Engine * CALL Create(int nKey); }