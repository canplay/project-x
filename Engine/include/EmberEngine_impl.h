#pragma once

#include "EmberEngine.h"
#include "EmberWindowManager.h"
#include "EmberRenderManager.h"
#include "EmberGUIManager.h"
#include "EmberInputManager.h"

namespace Ember
{
	class Engine_Impl : public Engine
	{
	public:
		bool CALL Run(LPCWSTR sTitle, HINSTANCE hInstance, void (*pRenderProc)(), HWND hWndParent = 0, int nWidth = 800, int nHeight = 600, bool bWindowed = true);
		void CALL Render();
		void CALL Close();

		void CALL AddLable(int nID, LPCWSTR sText, LPCWSTR sName, _VECTOR2 mPos = _VECTOR2(), _COLOR mColor = _COLOR());
		void CALL DeleteGUI(int nID);

		static Engine_Impl* _Interface_Get();

	private:
		Engine_Impl(void);

	private:
		_WINDOW m_Window;

		WCHAR m_sFPS[30];

		WindowManager* m_pWindowManager;
		RenderManager* m_pRenderManager;
		GUIManager* m_pGUIManager;
		InputManager* m_pInputManager;
	};
}

extern Ember::Engine_Impl* g_pEngine;