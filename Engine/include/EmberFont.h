#pragma once

#include <ft2build.h>
#include <freetype/freetype.h>
#include <freetype/ftglyph.h>
#include <freetype/fttypes.h>

namespace Ember
{
	class Font
	{
	public:
		Font(void);
		virtual ~Font(void);

		bool Create(LPCWSTR sFont, LPCWSTR sFile, int nSize, bool bOutLine);
		void Close();

		void Draw(LPCWSTR sText, _VECTOR2 mPos, _COLOR mColor, bool bWordWrap = true);
		void Render();

	private:
		int GetStringWidth(LPCWSTR sText, int nLen = -1);

		void FillTexture(WORD character);
		bool UpdateTexture(void* pTexture, void* pBuffer);

		void DrawStringToBuffer(LPCWSTR sText, _VECTOR2 mPos, _COLOR mColor, bool bWordWrap = true);

	private:
		// 每个字符的索引
		struct _CHARINDEX
		{
			bool bSet;          // 用于记录字符是否被缓冲
			BYTE texIdx;        // 纹理Index
			BYTE row, col;      // 纹理中字符的行列
			BYTE width, height; // 字宽高
			int	 delta_x, delta_y;	// 对齐偏移
		};

		// 要渲染的一个字符信息
		struct _CHARQUEUER
		{
			WORD  x, y;
			float z;
			WORD  width, height;
			float tx1, ty1;
			float tx2, ty2;
			DWORD color;
		};

		// 每次纹理上要渲染的字符
		struct _CHARTEXTURE
		{
			bool dirty;
			BYTE* buffer;
			void* pTexture;
			std::vector<_CHARQUEUER> pending;
		};

		static const int TEXTURE_WIDTH = 256;
		static const int TEXTURE_HEIGHT = 256;
		static const int TEXTURE_COUNT = 4;

		FT_Library m_FT2Lib;
		FT_Face m_FT_Face;

		_CHARINDEX	  m_Indices[256 * 256];      // 字符映射到纹理的位置
		_CHARTEXTURE  m_Textures[TEXTURE_COUNT]; // 字符贴图

		bool m_bOutLine; // 是否有阴影
		int  m_nCurrBufferIndex;    // 当前纹理缓冲的字符数
		int  m_nMaxCharBufferCount; // 最多支持的纹理缓冲字符数
		int	 m_nFontSize;	// 字体大小
		LPCWSTR m_sFontName;	// 字体名字
		LPCWSTR m_sFontFile;	// 字体文件
	};
}