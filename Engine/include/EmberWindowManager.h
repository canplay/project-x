#pragma once

namespace Ember
{
	class WindowManager : public Singleton<WindowManager>
	{
	public:
		WindowManager(void);
		virtual ~WindowManager(void);

		bool Create(_WINDOW mWindow);
		void Center();
		void Show(bool bShow) { ::ShowWindow(m_Window.hWnd, bShow ? SW_SHOW : SW_HIDE); }
		_WINDOW GetWindow() { return m_Window; }

		void Render();

		int GetFPS() { return m_nFPS; }
		void LockFPS(int nFPS) { m_dLockFPS = nFPS * 15; }

		static WindowManager& GetSingleton(void);
		static WindowManager* GetSingletonPtr(void);

	private:
		_WINDOW m_Window;

		WCHAR m_sFPS[10];
		float m_fTime, m_fDeltaTime;
		int m_nCurrentFPS, m_nFPS;
		DWORD m_dFixedDelta, m_dTime0, m_dTime0fps, m_dTime, m_dLockFPS;
	};
}