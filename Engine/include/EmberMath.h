#pragma once

namespace Ember
{
	struct _COLOR
	{
		_COLOR()
		{
			a = r = g = b = 1.0f;
		}
		_COLOR(float fa, float fr, float fg, float fb)
		{
			a = fa; r = fr; g = fg; b = fb;
		}

		float a, r, g, b;
	};

	struct _VECTOR2
	{
		_VECTOR2()
		{
			x = y = 0;
		}
		_VECTOR2(int nx, int ny)
		{
			x = nx; y = ny;
		}

		int x, y;
	};

	struct _VECTOR3
	{
		_VECTOR3()
		{
			x = y = z = 0;
		}
		_VECTOR3(int nx, int ny, int nz)
		{
			x = nx; y = ny; z = nz;
		}

		int x, y, z;
	};
}