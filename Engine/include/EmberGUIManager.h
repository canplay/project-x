#pragma once

#include <d3dx9.h>
#include <MyGUI.h>
#include <MyGUI_DirectXPlatform.h>

namespace Ember
{
	class GUIManager : public Singleton<GUIManager>
	{
	public:
		enum _INPUTSTATE { KEYDOWN, KEYUP, KEYPRESS, MOUSEDOWN, MOUSEUP, MOUSESCROLL };
		enum _GUITYPE { UILABLE, UIBUTTON };

	public:
		GUIManager(void);
		virtual ~GUIManager(void);

		bool Create();
		void Render();

		void AddLable(int nID, LPCWSTR sText, LPCWSTR sName, _VECTOR2 mPos = _VECTOR2(), _COLOR mColor = _COLOR());
		
		void DeleteGUI(int nID);

		static GUIManager& GetSingleton(void);
		static GUIManager* GetSingletonPtr(void);

	private:
		MyGUI::DirectXPlatform* m_pGUIPlatform;
		MyGUI::Gui* m_pGUI;
	};
}