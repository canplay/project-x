#pragma once

#include "EmberPrerequisites.h"
#include "EmberSingleton.h"
#include "EmberMath.h"

namespace Ember
{
	struct _WINDOW
	{
		_WINDOW()
		{
			hWnd = hWndParent = 0;
			hInstance = 0;
			nWidth = nHeight = 0;
			sTitle = L"Ember Engine";
			bWindowed = true;
			pRenderProc = 0;
		}

		HWND hWnd, hWndParent;
		HINSTANCE hInstance;
		int nWidth, nHeight;
		LPCWSTR sTitle;
		bool bWindowed;
		void (*pRenderProc)();
	};
}