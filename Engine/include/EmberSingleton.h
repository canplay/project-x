#pragma once

namespace Ember
{
    template <typename T> class Singleton
    {
    public:
        Singleton(void)
        {
            assert(!m_pSingleton);
			m_pSingleton = static_cast<T*>(this);
        }

        ~Singleton(void)
        {
			assert(m_pSingleton);
			m_pSingleton = 0;
		}

        static T& GetSingleton(void)
		{
			assert(m_pSingleton);
			return (*m_pSingleton);
		}
        static T* GetSingletonPtr(void)
		{
			return m_pSingleton;
		}

	private:
		Singleton(const Singleton<T> &);
		Singleton& operator=(const Singleton<T> &);

	protected:
		static T* m_pSingleton;
    };
}