#pragma once

#include "EmberDirect3D9Device.h"

namespace Ember
{
	class RenderManager : public Singleton<RenderManager>
	{
	public:
		enum _DEVICETYPE { DEVICE_NULL, DEVICE_DIRECT3D9, DEVICE_DIRECT3D10 };

	public:
		RenderManager(void);
		virtual ~RenderManager(void);

		bool Create(_DEVICETYPE mType);

		void StartRender();
		void EndRender();

		bool AddTexture(int nID, int nWidth, int nHeight, void* pTexture);
		void DeleteTexture(int nID);

		void DrawQuad(_VECTOR2 mSize, _VECTOR2 mPos);

		LPDIRECT3DDEVICE9 GetDevice() { return m_pDirect3D9->GetDevice(); }

		static RenderManager& GetSingleton(void);
		static RenderManager* GetSingletonPtr(void);

	private:
		_DEVICETYPE m_DeviceType;

		Direct3D9Device* m_pDirect3D9;
	};
}