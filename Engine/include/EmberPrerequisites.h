#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <timeapi.h>
#include <map>
#include <vector>

#ifdef ENGINE_EXPORTS
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

#define CALL __stdcall

#ifndef SAFE_DELETE
#define SAFE_DELETE(p)       { if (p) { delete (p);     (p) = NULL; } }
#endif    
#ifndef SAFE_DELETE_ARRAY
#define SAFE_DELETE_ARRAY(p) { if (p) { delete[] (p);   (p) = NULL; } }
#endif    
#ifndef SAFE_RELEASE
#define SAFE_RELEASE(p)      { if (p) { (p)->Release(); (p) = NULL; } }
#endif