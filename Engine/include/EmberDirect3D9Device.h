#pragma once

#include <d3d9.h>
#include <d3dx9.h>

namespace Ember
{
	class Direct3D9Device
	{
		struct _VERTEX2D
		{
			float x, y, z, rhw;
			D3DCOLOR diffuse;
		};

		struct _VERTEX3D
		{
			float x, y, z;
			float nx, ny, nz;
			float u, v;
			D3DCOLOR diffuse;
		};

	public:
		Direct3D9Device(void);
		virtual ~Direct3D9Device(void);

		bool Create(_WINDOW mWindow);

		void Begin();
		void End();

		bool AddTexture(int nID, int nWidth, int nHeight, void* pTexture);
		void DeleteTexture(int nID);

		void DrawQuad(_VECTOR2 mSize, _VECTOR2 mPos);

		LPDIRECT3DDEVICE9 GetDevice() { return m_pD3DDevice; }

	private:
		void InitDevice();

		void OnLostDevice();
		void OnResetDevice();

		void RenderProc(void (*pRenderProc)());

	private:
		LPDIRECT3D9			m_pD3D;
		LPDIRECT3DDEVICE9	m_pD3DDevice;
		D3DPRESENT_PARAMETERS m_D3Dpp;

		ID3DXSprite* m_pSprite;
		IDirect3DVertexBuffer9* m_pVertexBuffer;

		typedef std::map<int, void*> _TEXTUREMAP;
		_TEXTUREMAP m_TextureList;
	};
}
