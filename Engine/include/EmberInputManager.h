#pragma once

#include <MyGUI.h>
#include <OIS.h>

namespace Ember
{
	class InputManager : public Singleton<InputManager>, public OIS::MouseListener, public OIS::KeyListener
	{
	public:
		InputManager(void);
		virtual ~InputManager(void);

		bool Create(HWND hWnd);

// 		bool IsKeyDown(int nKey) { return m_pKeyboard->isKeyDown(nKey); }

		void CaptureInput();

		void SetInputViewSize(int nWidth, int nHeight);
		void SetMousePosition(int x, int y);

		void UpdateCursorPosition();

		static InputManager& GetSingleton(void);
		static InputManager* GetSingletonPtr(void);

	private:
		void CheckPosition();

		MyGUI::Char TranslateWin32Text(MyGUI::KeyCode key);

		bool mouseMoved(const OIS::MouseEvent& arg);
		bool mousePressed(const OIS::MouseEvent& arg, OIS::MouseButtonID nID);
		bool mouseReleased(const OIS::MouseEvent& arg, OIS::MouseButtonID nID);

		bool keyPressed(const OIS::KeyEvent& arg);
		bool keyReleased(const OIS::KeyEvent& arg);

	private:
		OIS::InputManager* m_pInputManager;
		OIS::Keyboard* m_pKeyboard;
		OIS::Mouse* m_pMouse;

		int m_nCursorX;
		int m_nCursorY;
	};
}