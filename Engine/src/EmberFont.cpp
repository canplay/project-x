#include "EmberStableHeaders.h"
#include "EmberFont.h"
#include "EmberRenderManager.h"

namespace Ember
{
	Font::Font(void)
	{
		m_nCurrBufferIndex = m_nMaxCharBufferCount = 0;

		m_bOutLine = false;
		m_FT2Lib  = 0;
		m_FT_Face = 0;
	}

	Font::~Font(void)
	{
	}

	bool Font::Create(LPCWSTR sFont, LPCWSTR sFile, int nSize, bool bOutLine)
	{
		m_sFontName = sFont;
		m_sFontFile = sFile;
		m_nFontSize = nSize;

		m_bOutLine = bOutLine;

		if (m_nFontSize <= 0)
			return false;

		m_nMaxCharBufferCount = (TEXTURE_WIDTH / m_nFontSize) * (TEXTURE_HEIGHT / m_nFontSize) * TEXTURE_COUNT;

		if (FT_Init_FreeType( &m_FT2Lib)) 
		{
			FT_Done_FreeType(m_FT2Lib);
			m_FT2Lib = NULL;
			return false;
		}

		char sTemp[MAX_PATH];
		WideCharToMultiByte(CP_ACP, 0, m_sFontFile, -1, sTemp, sizeof(sTemp), 0, 0);

		//加载一个字体,取默认的Face,一般为Regualer
		if (FT_New_Face(m_FT2Lib, sTemp, 0, &m_FT_Face))
		{
			FT_Done_FreeType(m_FT2Lib);
			m_FT2Lib = NULL;
			return false;
		}

		// 变换矩阵设为斜体 [11/8/2011 by 55]
		//float lean = 0.5f;
		//FT_Matrix matrix;
		//matrix.xx = 0x10000L;
		//matrix.xy = lean * 0x10000L;
		//matrix.yx = 0;
		//matrix.yy = 0x10000L;
		//FT_Set_Transform( m_FT_Face, &matrix, 0 );
		FT_Select_Charmap(m_FT_Face, FT_ENCODING_UNICODE);

		//大小要乘64.这是规定。照做就可以了。
		FT_Set_Pixel_Sizes(m_FT_Face, m_nFontSize, m_nFontSize);

		// 初始化Texture
		for (int i = 0; i < TEXTURE_COUNT; i++)
		{
			m_textures[i].dirty = false;
			m_textures[i].buffer = new BYTE[TEXTURE_WIDTH*TEXTURE_HEIGHT*4];
			memset(m_textures[i].buffer, 0, TEXTURE_WIDTH*TEXTURE_HEIGHT*4);
			m_textures[i].pending.clear();

			if (RenderManager::GetSingleton().AddTexture(i, TEXTURE_WIDTH, TEXTURE_HEIGHT, m_textures[i].pTexture));
			{
				FT_Done_FreeType(m_FT2Lib);
				m_FT2Lib = NULL;
				return false;
			}
		}

		return true;
	}

	void Font::Close()
	{
		for (int i = 0; i < TEXTURE_COUNT; i++)
		{
			RenderManager::GetSingleton().DeleteTexture(i);
			delete[] m_Textures[i].pTexture;
		}

		if(m_FT2Lib)
			FT_Done_FreeType(m_FT2Lib);
	}

	void Font::Draw(LPCWSTR sText, _VECTOR2 mPos, _COLOR mColor, bool bWordWrap /*= true*/)
	{
		int StringWidth = GetStringWidth(sText, wcslen(sText));
		int StringHeight = m_nFontSize;

		DrawStringToBuffer(sText, mPos, mColor, bWordWrap);
	}

	int Font::GetStringWidth(LPCWSTR sText, int nLen)
	{
		int nWidth = 0;
		int count = 0;
		WORD character = 0;

		char* p;
		WideCharToMultiByte(CP_ACP, 0, sText, -1, p, sizeof(p), 0, 0);
		while (*p)
		{
			if (nLen != -1 && nLen <= count)
				break;

			character = *(WORD*)p;
			p		+= 2;
			count	+= 2;

			const _CHARINDEX& index = m_Indices[character];
			FillTexture(character);
			nWidth += index.width;
		}

		return nWidth;
	}

	void Font::FillTexture(WORD character)
	{
		if (m_Indices[character].bSet)
			return;

		// 判断贴图缓冲区满的情况
		m_nCurrBufferIndex ++;
		if (m_nCurrBufferIndex >= m_nMaxCharBufferCount)
		{
			Rander();
			_ResetTextureBuffer();
		}

		// 分配位置
		int CharPerLine = TEXTURE_WIDTH / m_nFontSize;
		int CharPerCol = TEXTURE_HEIGHT / m_nFontSize;
		int CharPerTexture = CharPerLine * CharPerCol;

		int texIdx = m_nCurrBufferIndex / CharPerTexture;
		int row = (m_nCurrBufferIndex % CharPerTexture) / CharPerLine;
		int col = (m_nCurrBufferIndex % CharPerTexture) % CharPerCol;

		_CHARINDEX& indices = m_Indices[character];
		indices.bSet	= true;
		indices.texIdx	= texIdx;
		indices.row		= row;
		indices.col		= col;

		LPCSTR chr = (LPCSTR)&character;
		wchar_t wchar;
		// 转成wchar_t;
		MultiByteToWideChar( CP_ACP, 0, chr, strlen(chr) + 1, &wchar,1 );
		if (FT_Load_Char(m_FT_Face, wchar, FT_LOAD_RENDER | FT_LOAD_TARGET_LIGHT))
			return;

		FT_GlyphSlot slot = m_FT_Face->glyph;
		FT_Bitmap bitmap = slot->bitmap;

		//把位图数据拷贝自己定义的数据区里.这样旧可以画到需要的东西上面了。
		int width  =  bitmap.width;
		int height =  bitmap.rows;

		indices.width = m_FT_Face->glyph->advance.x / 64;
		indices.height = m_FT_Face->size->metrics.y_ppem;
		indices.delta_x = (slot->metrics.horiBearingX >> 6);
		indices.delta_y = (slot->bitmap_top - m_nFontSize);

		int dstStride = TEXTURE_WIDTH * 4;
		int dstbegin = row * m_nFontSize * dstStride + col * m_nFontSize * 4;
		unsigned char* src = bitmap.buffer;
		BYTE* pBuf = m_Textures[texIdx].buffer;

		switch (m_FT_Face->glyph->bitmap.pixel_mode)
		{
		case FT_PIXEL_MODE_GRAY:
			{
				for (int j = 0; j  < height ; j++)
				{
					for (int i = 0; i < width; i++)
					{
						unsigned char _vl =  src[i + bitmap.width*j];

						pBuf[dstbegin + (4*i + j * dstStride)  ] = 0xFF;
						pBuf[dstbegin + (4*i + j * dstStride)+1] = 0xFF;
						pBuf[dstbegin + (4*i + j * dstStride)+2] = 0xFF;
						pBuf[dstbegin + (4*i + j * dstStride)+3] = _vl;
					}
				}
			}
			break;

		case FT_PIXEL_MODE_MONO:
			{	
				for (int i = 0; i  < height ; i++)
				{
					for (int j = 0; j < width; j++)
					{
						unsigned char _vl = 0;
						if (src[i * bitmap.pitch + j / 8] & (0x80 >> (j & 7)))
							_vl = 0xFF;
						else
							_vl = 0x00;

						pBuf[dstbegin + (4*j + i * dstStride)  ] = 0xFF;
						pBuf[dstbegin + (4*j + i * dstStride)+1] = 0xFF;
						pBuf[dstbegin + (4*j + i * dstStride)+2] = 0xFF;
						pBuf[dstbegin + (4*j + i * dstStride)+3] = _vl;
					}
				}
			}
			break;
		}

		m_Textures[texIdx].dirty = true;
	}

	void Font::Render()
	{
		float sx 	= 0;
		float sy 	= 0;
		float rsx	= 0;
		float rsy	= 0;
		float tx1	= 0;
		float ty1	= 0;
		float tx2	= 0;
		float ty2	= 0;

		for (int i = 0; i<TEXTURE_COUNT; i++)
		{
			_CHARTEXTURE* pTexture = &(m_Textures[i]);
			if (pTexture->pending.empty())
				continue;

			int numVerts = (int)pTexture->pending.size() * 3 * 2 * (m_bOutLine?2:1);

			FontVertex* verts;
			LPDIRECT3DVERTEXBUFFER9 vertices;
			m_pRtgDevice->CreateVertexBuffer(numVerts* sizeof(FontVertex), 0, VERTEX_FVF, D3DPOOL_DEFAULT, &vertices, NULL);
			vertices->Lock(0, 0, (void**) &verts, 0);

			for (std::vector<_CHARQUEUER>::iterator queuer = pTexture->pending.begin(); 
				queuer != pTexture->pending.end(); queuer++)
			{
				sx  = queuer->x - 0.5f;
				sy  = queuer->y - 0.5f;
				rsx = sx + queuer->width;
				rsy = sy + queuer->height;
				tx1 = queuer->tx1;
				ty1 = queuer->ty1;
				tx2 = queuer->tx2;
				ty2 = queuer->ty2;

				if (m_bOutLine)
				{
					*verts++ = FontVertex(sx + 1,	rsy + 1,	queuer->z, 0x00000000, tx1, ty2);
					*verts++ = FontVertex(sx + 1,	sy + 1,		queuer->z, 0x00000000, tx1, ty1);
					*verts++ = FontVertex(rsx + 1,	rsy + 1,	queuer->z, 0x00000000, tx2, ty2);
					*verts++ = FontVertex(rsx + 1,	sy + 1,		queuer->z, 0x00000000, tx2, ty1);
					*verts++ = FontVertex(rsx + 1,	rsy + 1,	queuer->z, 0x00000000, tx2, ty2);
					*verts++ = FontVertex(sx + 1,	sy + 1,		queuer->z, 0x00000000, tx1, ty1);
				}

				*verts++ = FontVertex(sx,	rsy,queuer->z, queuer->color, tx1, ty2);
				*verts++ = FontVertex(sx,	sy,	queuer->z, queuer->color, tx1, ty1);
				*verts++ = FontVertex(rsx,	rsy,queuer->z, queuer->color, tx2, ty2);
				*verts++ = FontVertex(rsx,	sy,	queuer->z, queuer->color, tx2, ty1);
				*verts++ = FontVertex(rsx,	rsy,queuer->z, queuer->color, tx2, ty2);
				*verts++ = FontVertex(sx,	sy,	queuer->z, queuer->color, tx1, ty1);
			}

			vertices->Unlock();
			pTexture->pending.resize(0);
			if (pTexture->dirty)
			{
				UpdateTexture(pTexture->pTexture, pTexture->buffer);
				pTexture->dirty = false;
			}

			m_pRtgDevice->SetStreamSource(0, vertices, 0, sizeof(FontVertex));
			m_pRtgDevice->SetFVF(VERTEX_FVF);	
			m_pRtgDevice->SetTexture(0, pTexture->pTexture);
			m_pRtgDevice->DrawPrimitive(D3DPT_TRIANGLELIST, 0, numVerts / 3);
		}
	}

	bool Font::UpdateTexture(void* pTexture, void* pBuffer)
	{
		for (int y = 0; y < TEXTURE_HEIGHT; ++y)
		{
			for (int x = 0; x < TEXTURE_WIDTH; ++x)
			{
				byte* source_pixel = (byte*)pBuffer + (TEXTURE_WIDTH * 4 * y) + (x * 4);
				byte* destination_pixel = ((byte*) desc.pBits) + desc.Pitch * y + x * 4;

				destination_pixel[0] = source_pixel[2];
				destination_pixel[1] = source_pixel[1];
				destination_pixel[2] = source_pixel[0];
				destination_pixel[3] = source_pixel[3];
			}
		}
	}
}