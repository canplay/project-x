#include "EmberStableHeaders.h"
#include "EmberGUIManager.h"
#include "EmberRenderManager.h"

namespace Ember
{
	template<> GUIManager* Singleton<GUIManager>::m_pSingleton = 0;
	GUIManager* GUIManager::GetSingletonPtr(void)
	{
		return m_pSingleton;
	}
	GUIManager& GUIManager::GetSingleton(void)
	{  
		assert(m_pSingleton);
		return(*m_pSingleton);  
	}

	GUIManager::GUIManager(void)
	{
	}

	bool GUIManager::Create()
	{
		m_pGUIPlatform = new MyGUI::DirectXPlatform();
		m_pGUIPlatform->initialise(RenderManager::GetSingleton().GetDevice());
		m_pGUIPlatform->getDataManagerPtr()->addResourceLocation("../Bin/Media/Setting", false);
		m_pGUIPlatform->getDataManagerPtr()->addResourceLocation("../Bin/Media/Layout", false);

		m_pGUI = new MyGUI::Gui();
		m_pGUI->initialise("Core.xml");

		const MyGUI::VectorWidgetPtr& root = MyGUI::LayoutManager::getInstance().loadLayout("EditPanel.layout");
// 		if (root.size() == 1)
// 			root.at(0)->findWidget("_Main")->castType<MyGUI::Window>()->setCaption("Test");

		return true;
	}

	GUIManager::~GUIManager(void)
	{
		m_pGUI->shutdown();
		SAFE_DELETE(m_pGUI);
		m_pGUIPlatform->shutdown();
		SAFE_DELETE(m_pGUIPlatform);
	}

	void GUIManager::Render()
	{
		m_pGUIPlatform->getRenderManagerPtr()->drawOneFrame();
	}

	void GUIManager::AddLable(int nID, LPCWSTR sText, LPCWSTR sName, _VECTOR2 mPos, _COLOR mColor)
	{
	}

	void GUIManager::DeleteGUI(int nID)
	{
	}
}