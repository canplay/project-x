#include "EmberStableHeaders.h"
#include "EmberDirect3D9Device.h"
#include "EmberWindowManager.h"

#define D3DFVF_VERTEX2D (D3DFVF_XYZRHW | D3DFVF_DIFFUSE | D3DFVF_TEX1)
#define D3DFVF_VERTEX3D (D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_DIFFUSE)

namespace Ember
{
	Direct3D9Device::Direct3D9Device(void)
	{
		m_pD3D = 0;
		m_pD3DDevice = 0;
		m_pSprite = 0;
		m_pVertexBuffer = 0;
	}

	Direct3D9Device::~Direct3D9Device(void)
	{
		_TEXTUREMAP::const_iterator it = m_TextureList.begin();
		while (it != m_TextureList.end())
			delete it->second;

		SAFE_RELEASE(m_pVertexBuffer);
		SAFE_RELEASE(m_pSprite);
		SAFE_RELEASE(m_pD3DDevice);
		SAFE_RELEASE(m_pD3D);
	}

	bool Direct3D9Device::Create(_WINDOW mWindow)
	{
		if (NULL == (m_pD3D = Direct3DCreate9(D3D_SDK_VERSION))) return false; // D3D_SDK_VERSION

		D3DDISPLAYMODE displayMode;
		if (FAILED(m_pD3D->GetAdapterDisplayMode(D3DADAPTER_DEFAULT, &displayMode))) return false;

		ZeroMemory(&m_D3Dpp, sizeof(m_D3Dpp));

		if (mWindow.bWindowed)
			m_D3Dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
		else
			m_D3Dpp.SwapEffect = D3DSWAPEFFECT_FLIP;

		m_D3Dpp.Windowed				= mWindow.bWindowed;
		m_D3Dpp.BackBufferWidth			= mWindow.nWidth;
		m_D3Dpp.BackBufferHeight		= mWindow.nHeight;
		m_D3Dpp.BackBufferFormat		= displayMode.Format;
		m_D3Dpp.BackBufferCount			= 1;
		m_D3Dpp.MultiSampleType			= D3DMULTISAMPLE_NONE;
		m_D3Dpp.hDeviceWindow			= mWindow.hWnd;
		m_D3Dpp.EnableAutoDepthStencil	= TRUE;
		m_D3Dpp.AutoDepthStencilFormat	= D3DFMT_D24S8;
		m_D3Dpp.Flags					= 0;
		m_D3Dpp.FullScreen_RefreshRateInHz = D3DPRESENT_RATE_DEFAULT;

		if (mWindow.bWindowed)
			m_D3Dpp.PresentationInterval = D3DPRESENT_INTERVAL_IMMEDIATE;
		else
			m_D3Dpp.PresentationInterval = D3DPRESENT_INTERVAL_ONE;

		if (FAILED(m_pD3D->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, mWindow.hWnd,
			D3DCREATE_HARDWARE_VERTEXPROCESSING, &m_D3Dpp, &m_pD3DDevice)))
			return false;

		if (FAILED(D3DXCreateSprite(m_pD3DDevice, &m_pSprite)))
			return false;

		InitDevice();

		return true;
	}

	void Direct3D9Device::InitDevice()
	{
		m_pD3DDevice->SetRenderState(D3DRS_ZENABLE, TRUE);
		m_pD3DDevice->SetRenderState(D3DRS_AMBIENT, 0xffffffff);
		m_pD3DDevice->SetRenderState(D3DRS_ALPHATESTENABLE, TRUE);
		m_pD3DDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
		m_pD3DDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
		m_pD3DDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
		m_pD3DDevice->SetRenderState(D3DRS_ALPHAREF, 0x08);
		m_pD3DDevice->SetRenderState(D3DRS_ALPHAFUNC, D3DCMP_GREATEREQUAL);
		m_pD3DDevice->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_SELECTARG1);

		m_pD3DDevice->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
		m_pD3DDevice->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
		m_pD3DDevice->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_POINT);
	}

	void Direct3D9Device::OnLostDevice()
	{

	}

	void Direct3D9Device::OnResetDevice()
	{
		if (FAILED(m_pD3DDevice->Reset(&m_D3Dpp))) return;

		m_pSprite->OnResetDevice();

		InitDevice();
	}

	void Direct3D9Device::Begin()
	{
		m_pD3DDevice->Clear(0, 0, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER | D3DCLEAR_STENCIL, D3DCOLOR_XRGB(0, 0, 255), 1.0f, 0);
		m_pD3DDevice->BeginScene();
		m_pSprite->Begin(D3DXSPRITE_ALPHABLEND);

		if (m_pVertexBuffer)
		{
			m_pD3DDevice->SetStreamSource(0, m_pVertexBuffer, 0, sizeof(_VERTEX2D));
			m_pD3DDevice->SetFVF(D3DFVF_VERTEX2D);
			m_pD3DDevice->DrawPrimitive(D3DPT_TRIANGLESTRIP, 0, 2);
		}
	}

	void Direct3D9Device::End()
	{
		m_pSprite->End();
		m_pD3DDevice->EndScene();
		if (D3DERR_DEVICELOST == m_pD3DDevice->Present(0, 0, 0, 0))
		{
			if (m_pD3DDevice->TestCooperativeLevel() == D3DERR_DEVICENOTRESET)
			{
				OnLostDevice();
				OnResetDevice();
			}
		}
	}

	void Direct3D9Device::DrawQuad(_VECTOR2 mSize, _VECTOR2 mPos)
	{
		_VERTEX2D objData[] =
		{
			{ (float)mPos.x, (float)mPos.y, 0.5f, 1, D3DCOLOR_XRGB(255, 255, 255), },
			{ (float)mPos.x, float(mPos.y + mSize.y), 0.5f, 1, D3DCOLOR_XRGB(255, 255, 255), },
			{ float(mPos.x + mSize.x), (float)mPos.y, 0.5f, 1, D3DCOLOR_XRGB(255, 255, 255), },
			{ float(mPos.x + mSize.x), float(mPos.y + mSize.y), 0.5f, 1, D3DCOLOR_XRGB(255, 255, 255), },
		};

		if (FAILED(m_pD3DDevice->CreateVertexBuffer(sizeof(objData), 0, D3DFVF_VERTEX2D, D3DPOOL_DEFAULT, &m_pVertexBuffer, NULL)))
			return;

		void* pVertex = 0;
		if (FAILED(m_pVertexBuffer->Lock(0, sizeof(objData), (void**)&pVertex, 0))) return;
		memcpy(pVertex, objData, sizeof(objData));
		m_pVertexBuffer->Unlock();
	}

	bool Direct3D9Device::AddTexture(int nID, int nWidth, int nHeight, void* pTexture)
	{
		if (m_pD3DDevice->CreateTexture(nWidth, nHeight, 1, 0, D3DFMT_A8R8G8B8, D3DPOOL_MANAGED, (LPDIRECT3DTEXTURE9*)pTexture, NULL) != D3D_OK)
			return false;

		m_TextureList.insert(std::make_pair(nID, pTexture));

		return true;
	}

	void Direct3D9Device::DeleteTexture(int nID)
	{
		_TEXTUREMAP::const_iterator it = m_TextureList.find(nID);
		if (it != m_TextureList.end())
		{
			delete it->second;
			ZeroMemory(it->second, sizeof(it->second));
			m_TextureList.erase(it);
		}
	}
}
