#include "EmberStableHeaders.h"
#include "EmberInputManager.h"

namespace Ember
{
	template<> InputManager* Singleton<InputManager>::m_pSingleton = 0;
	InputManager* InputManager::GetSingletonPtr(void)
	{
		return m_pSingleton;
	}
	InputManager& InputManager::GetSingleton(void)
	{  
		assert(m_pSingleton);
		return(*m_pSingleton);  
	}

	InputManager::InputManager(void)
	{
		m_nCursorX = m_nCursorY = 0;
	}

	InputManager::~InputManager(void)
	{
		if (m_pInputManager)
		{
			if (m_pMouse)
			{
				m_pInputManager->destroyInputObject(m_pMouse);
				m_pMouse = 0;
			}

			if (m_pKeyboard)
			{
				m_pInputManager->destroyInputObject(m_pKeyboard);
				m_pKeyboard = 0;
			}

			OIS::InputManager::destroyInputSystem(m_pInputManager);
			m_pInputManager = 0;
		}
	}

	bool InputManager::Create(HWND hWnd)
	{
		char sHandle[MAX_PATH];
		sprintf_s(sHandle, "%d", hWnd);
		
		OIS::ParamList pl;
		pl.insert(std::make_pair(std::string("WINDOW"), sHandle));

		pl.insert(std::make_pair(std::string("w32_mouse"), std::string("DISCL_FOREGROUND" )));
		pl.insert(std::make_pair(std::string("w32_mouse"), std::string("DISCL_NONEXCLUSIVE")));
		pl.insert(std::make_pair(std::string("w32_keyboard"), std::string("DISCL_FOREGROUND")));
		pl.insert(std::make_pair(std::string("w32_keyboard"), std::string("DISCL_NONEXCLUSIVE")));

		m_pInputManager = OIS::InputManager::createInputSystem(pl);
		if (!m_pInputManager) return false;

		m_pKeyboard = static_cast<OIS::Keyboard*>(m_pInputManager->createInputObject(OIS::OISKeyboard, true));
		m_pKeyboard->setEventCallback(this);

		m_pMouse = static_cast<OIS::Mouse*>(m_pInputManager->createInputObject(OIS::OISMouse, true));
		m_pMouse->setEventCallback(this);

		return true;
	}

	void InputManager::CheckPosition()
	{
		const OIS::MouseState& ms = m_pMouse->getMouseState();

		if (m_nCursorX < 0) m_nCursorX = 0;
		else if (m_nCursorX >= ms.width)
			m_nCursorX = ms.width - 1;

		if (m_nCursorY < 0) m_nCursorY = 0;
		else if (m_nCursorY >= ms.height)
			m_nCursorY = ms.height - 1;
	}

	void InputManager::CaptureInput()
	{
		if (m_pMouse) m_pMouse->capture();
		m_pKeyboard->capture();
	}

	void InputManager::SetInputViewSize(int nWidth, int nHeight)
	{
		if (m_pMouse)
		{
			const OIS::MouseState& ms = m_pMouse->getMouseState();
			ms.width = nWidth;
			ms.height = nHeight;

			CheckPosition();
		}
	}

	void InputManager::SetMousePosition(int x, int y)
	{
		m_nCursorX = x;
		m_nCursorY = y;

		CheckPosition();
	}

	void InputManager::UpdateCursorPosition()
	{
		const OIS::MouseState& ms = m_pMouse->getMouseState();
		MyGUI::InputManager::getInstance().injectMouseMove(ms.X.abs, ms.Y.abs, ms.Z.abs);
	}

	MyGUI::Char InputManager::TranslateWin32Text(MyGUI::KeyCode key)
	{
		static WCHAR deadKey = 0;

		BYTE keyState[256];
		HKL  layout = GetKeyboardLayout(0);
		if (GetKeyboardState(keyState) == 0)
			return 0;

		int code = *((int*)&key);
		unsigned int vk = MapVirtualKeyEx((UINT)code, 3, layout);
		if (vk == 0) return 0;

		WCHAR buff[3] = { 0, 0, 0 };
		int ascii = ToUnicodeEx(vk, (UINT)code, keyState, buff, 3, 0, layout);
		if (ascii == 1 && deadKey != '\0')
		{
			WCHAR wcBuff[3] = { buff[0], deadKey, '\0' };
			WCHAR out[3];

			deadKey = '\0';
			if (FoldStringW(MAP_PRECOMPOSED, (LPWSTR)wcBuff, 3, (LPWSTR)out, 3))
				return out[0];
		}
		else if (ascii == 1)
		{
			deadKey = '\0';
			return buff[0];
		}
		else if (ascii == 2)
		{
			switch (buff[0])
			{
			case 0x5E:
				deadKey = 0x302;
				break;
			case 0x60:
				deadKey = 0x300;
				break;
			case 0xA8:
				deadKey = 0x308;
				break;
			case 0xB4:
				deadKey = 0x301;
				break;
			case 0xB8:
				deadKey = 0x327;
				break;
			default:
				deadKey = buff[0];
				break;
			}
		}

		return 0;
	}

	bool InputManager::mouseMoved(const OIS::MouseEvent& arg)
	{
		m_nCursorX = arg.state.X.abs;
		m_nCursorY = arg.state.Y.abs;

		CheckPosition();

		MyGUI::InputManager::getInstance().injectMouseMove(arg.state.X.abs, arg.state.Y.abs, arg.state.Z.abs);
		return true;
	}

	bool InputManager::mousePressed(const OIS::MouseEvent& arg, OIS::MouseButtonID id)
	{
		MyGUI::InputManager::getInstance().injectMousePress(arg.state.X.abs, arg.state.Y.abs, MyGUI::MouseButton::Enum(id));
		return true;
	}

	bool InputManager::mouseReleased(const OIS::MouseEvent& arg, OIS::MouseButtonID id)
	{
		MyGUI::InputManager::getInstance().injectMouseRelease(arg.state.X.abs, arg.state.Y.abs, MyGUI::MouseButton::Enum(id));
		return true;
	}

	bool InputManager::keyPressed(const OIS::KeyEvent& arg)
	{
		char keyboadname[9];  
		GetKeyboardLayoutNameA(keyboadname);  

		if (!strcmp("00000804", keyboadname))      
		{
			MyGUI::Char text = (MyGUI::Char)arg.text;
			MyGUI::KeyCode key = MyGUI::KeyCode::Enum(arg.key);
			int scan_code = key.getValue();

			if (scan_code > 70 && scan_code < 84)
			{
				static MyGUI::Char nums[13] = { 55, 56, 57, 45, 52, 53, 54, 43, 49, 50, 51, 48, 46 };
				text = nums[scan_code - 71];
			}
			else if (key == MyGUI::KeyCode::Divide)
				text = '/';
			else
				text = TranslateWin32Text(key);

			MyGUI::InputManager::getInstance().injectKeyPress(MyGUI::KeyCode::Enum(arg.key), arg.text);
		}
		return true;
	}

	bool InputManager::keyReleased(const OIS::KeyEvent& arg)
	{
		MyGUI::InputManager::getInstance().injectKeyRelease(MyGUI::KeyCode::Enum(arg.key));
		return true;
	}
}