#include "EmberStableHeaders.h"
#include "EmberEngine_impl.h"
#if defined(DEBUG) || defined(_DEBUG)
#include "vld.h"
#endif

int g_nRef = 0;
Ember::Engine_Impl* g_pEngine = 0;


BOOL APIENTRY DllMain(HMODULE hModule, DWORD ul_reason_for_call, LPVOID lpReserved)
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
	case DLL_PROCESS_DETACH:
		break;
	}
	return TRUE;
}

Ember::Engine* CALL Create(int mKey)
{
	if (mKey == 123)
		return (Ember::Engine*)Ember::Engine_Impl::_Interface_Get();
	else
		return 0;
}

namespace Ember
{
	Engine_Impl* Engine_Impl::_Interface_Get()
	{
		if (!g_pEngine) g_pEngine = new Engine_Impl();
		g_nRef++;
		return g_pEngine;
	}

	Engine_Impl::Engine_Impl()
	{
	}

	bool CALL Engine_Impl::Run(LPCWSTR sTitle, HINSTANCE hInstance, void (*pRenderProc)(), HWND hWndParent, int nWidth, int nHeight, bool bWindowed)
	{
		m_Window.nWidth = nWidth;
		m_Window.nHeight = nHeight;
		m_Window.sTitle = sTitle;
		m_Window.bWindowed = bWindowed;
		m_Window.hWndParent = hWndParent;
		m_Window.hInstance = hInstance;
		m_Window.pRenderProc = pRenderProc;

		m_pWindowManager = new WindowManager();
		if (!m_pWindowManager->Create(m_Window)) return false;

		m_pRenderManager = new RenderManager();
		if (!m_pRenderManager->Create(RenderManager::DEVICE_DIRECT3D9)) return false;

		m_pInputManager = new InputManager();
		if (!m_pInputManager->Create(m_pWindowManager->GetWindow().hWnd)) return false;
		m_pInputManager->SetInputViewSize(m_Window.nWidth, m_Window.nHeight);

		m_pGUIManager = new GUIManager();
		if (!m_pGUIManager->Create()) return false;

		m_pWindowManager->Center();
		
		m_pRenderManager->DrawQuad(_VECTOR2(100, 100), _VECTOR2(400, 300));

		return true;
	}

	void CALL Engine_Impl::Render()
	{
		m_pWindowManager->Render();
	}

	void CALL Engine_Impl::Close()
	{
		SAFE_DELETE(m_pInputManager);
		SAFE_DELETE(m_pGUIManager);
		SAFE_DELETE(m_pRenderManager);
		SAFE_DELETE(m_pWindowManager);

		g_nRef--;
		if (!g_nRef) SAFE_DELETE(g_pEngine);
	}

	void CALL Engine_Impl::AddLable(int nID, LPCWSTR sText, LPCWSTR sName, _VECTOR2 mPos, _COLOR mColor)
	{
		GUIManager::GetSingleton().AddLable(nID, sText, sName, mPos, mColor);
	}

	void CALL Engine_Impl::DeleteGUI(int nID)
	{
		GUIManager::GetSingleton().DeleteGUI(nID);
	}
}
