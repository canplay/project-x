#include "EmberStableHeaders.h"
#include "EmberRenderManager.h"
#include "EmberWindowManager.h"

namespace Ember
{
	template<> RenderManager* Singleton<RenderManager>::m_pSingleton = 0;
	RenderManager* RenderManager::GetSingletonPtr(void)
	{
		return m_pSingleton;
	}
	RenderManager& RenderManager::GetSingleton(void)
	{  
		assert(m_pSingleton);
		return(*m_pSingleton);  
	}

	RenderManager::RenderManager(void)
	{
	}

	RenderManager::~RenderManager(void)
	{
		switch (m_DeviceType)
		{
		case DEVICE_DIRECT3D9:
			SAFE_DELETE(m_pDirect3D9);

		case DEVICE_DIRECT3D10:
			break;
		}
	}

	bool RenderManager::Create(_DEVICETYPE mType)
	{
		m_DeviceType = mType;

		switch (m_DeviceType)
		{
		case DEVICE_DIRECT3D9:
			m_pDirect3D9 = new Direct3D9Device();
			return m_pDirect3D9->Create(WindowManager::GetSingleton().GetWindow());

		case DEVICE_DIRECT3D10:
			break;
		}

		return false;
	}

	void RenderManager::StartRender()
	{
		switch (m_DeviceType)
		{
		case DEVICE_DIRECT3D9:
			m_pDirect3D9->Begin();
			break;

		case DEVICE_DIRECT3D10:
			break;
		}
	}

	void RenderManager::EndRender()
	{
		switch (m_DeviceType)
		{
		case DEVICE_DIRECT3D9:
			m_pDirect3D9->End();
			break;

		case DEVICE_DIRECT3D10:
			break;
		}
	}

	void RenderManager::DrawQuad(_VECTOR2 mSize, _VECTOR2 mPos)
	{
		switch (m_DeviceType)
		{
		case DEVICE_DIRECT3D9:
			m_pDirect3D9->DrawQuad(mSize, mPos);
			break;

		case DEVICE_DIRECT3D10:
			break;
		}
	}

	bool RenderManager::AddTexture(int nID, int nWidth, int nHeight, void* pTexture)
	{
		switch (m_DeviceType)
		{
		case DEVICE_DIRECT3D9:
			return m_pDirect3D9->AddTexture(nID, nWidth, nHeight, pTexture);

		case DEVICE_DIRECT3D10:
			return false;

		default:
			return false;
		}
	}

	void RenderManager::DeleteTexture(int nID)
	{
		switch (m_DeviceType)
		{
		case DEVICE_DIRECT3D9:
			m_pDirect3D9->DeleteTexture(nID);
			break;

		case DEVICE_DIRECT3D10:
			break;
		}
	}
}