#include "EmberStableHeaders.h"
#include "EmberWindowManager.h"
#include "EmberRenderManager.h"
#include "EmberGUIManager.h"
#include "EmberInputManager.h"

#define WINDOW_CLASS_NAME L"EmberEngineApplication"

LRESULT CALLBACK WindowProc(HWND hWnd, UINT nMsg, WPARAM wParam, LPARAM lParam)
{
	switch(nMsg)
	{
	case WM_CREATE: 
		return FALSE;

	case WM_DESTROY:
		PostQuitMessage(0);
		return FALSE;
	}

	return DefWindowProc(hWnd, nMsg, wParam, lParam);
}

namespace Ember
{
	template<> WindowManager* Singleton<WindowManager>::m_pSingleton = 0;
	WindowManager* WindowManager::GetSingletonPtr(void)
	{
		return m_pSingleton;
	}
	WindowManager& WindowManager::GetSingleton(void)
	{  
		assert(m_pSingleton);
		return(*m_pSingleton);  
	}

	WindowManager::WindowManager(void)
	{
	}

	WindowManager::~WindowManager(void)
	{
		timeEndPeriod(1);

		if (m_Window.hWnd)
		{
			::DestroyWindow(m_Window.hWnd);
			m_Window.hWnd = 0;
		}

		if (m_Window.hInstance) UnregisterClass(WINDOW_CLASS_NAME, m_Window.hInstance);
	}

	bool WindowManager::Create(_WINDOW mWindow)
	{
		m_Window = mWindow;

		WNDCLASS wndclass;
		wndclass.style = CS_DBLCLKS | CS_OWNDC | CS_HREDRAW | CS_VREDRAW;
		wndclass.lpfnWndProc	= WindowProc;
		wndclass.cbClsExtra		= 0;
		wndclass.cbWndExtra		= 0;
		wndclass.hInstance		= m_Window.hInstance;
		wndclass.hCursor		= LoadCursor(NULL, IDC_ARROW);
		wndclass.hbrBackground	= (HBRUSH)GetStockObject(BLACK_BRUSH);
		wndclass.lpszMenuName	= NULL; 
		wndclass.lpszClassName	= WINDOW_CLASS_NAME;
		wndclass.hIcon = LoadIcon(m_Window.hInstance, IDI_APPLICATION);

		if (!RegisterClass(&wndclass))
		{
			MessageBox(0, L"无法注册窗口类！", L"错误", MB_OK);
			return false;
		}

		int nWidth, nHeight;
		nWidth = m_Window.nWidth + GetSystemMetrics(SM_CXFIXEDFRAME) * 2;
		nHeight = m_Window.nHeight + GetSystemMetrics(SM_CYFIXEDFRAME) * 2 + GetSystemMetrics(SM_CYCAPTION);

		RECT rectWindow, rectFull;

		rectWindow.left = (GetSystemMetrics(SM_CXSCREEN) - m_Window.nWidth) / 2;
		rectWindow.top = (GetSystemMetrics(SM_CYSCREEN) - m_Window.nHeight) / 2;
		rectWindow.right = rectWindow.left + m_Window.nWidth;
		rectWindow.bottom = rectWindow.top + m_Window.nHeight;
		LONG styleWindow = WS_POPUP | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX | WS_VISIBLE;

		rectFull.left = 0;
		rectFull.top = 0;
		rectFull.right = nWidth;
		rectFull.bottom = nHeight;
		LONG styleFull = WS_POPUP | WS_VISIBLE;

		if (m_Window.hWndParent)
		{
			rectWindow.left = rectWindow.top = 0;
			rectWindow.right = nWidth;
			rectWindow.bottom = nHeight;
			styleWindow = WS_CHILD | WS_VISIBLE; 
			m_Window.bWindowed = true;
		}

		if (m_Window.bWindowed)
			m_Window.hWnd = CreateWindowEx(0, WINDOW_CLASS_NAME, m_Window.sTitle, styleWindow,
			rectWindow.right + rectWindow.left, rectWindow.bottom + rectWindow.top,
			rectWindow.right - rectWindow.left, rectWindow.bottom - rectWindow.top,
			m_Window.hWndParent, NULL, m_Window.hInstance, NULL);
		else
			m_Window.hWnd = CreateWindowEx(WS_EX_TOPMOST, WINDOW_CLASS_NAME, m_Window.sTitle, styleFull,
			0, 0, 0, 0, NULL, NULL, m_Window.hInstance, NULL);

		if (!m_Window.hWnd)
		{
			MessageBox(0, L"无法创建窗口！", L"错误", MB_OK);
			return false;
		}

		if (m_Window.bWindowed) m_dLockFPS = 30;
		else m_dLockFPS = 1000;

		timeBeginPeriod(1);
		m_dTime0 = m_dTime0fps = timeGetTime();
		m_fTime = 0.0f;
		m_dTime = m_nCurrentFPS = m_nFPS= m_dFixedDelta = 0;

		return true;
	}

	void WindowManager::Center()
	{
		int nWidth, nHeight;
		nWidth = m_Window.nWidth + GetSystemMetrics(SM_CXFIXEDFRAME) * 2;
		nHeight = m_Window.nHeight + GetSystemMetrics(SM_CYFIXEDFRAME) * 2 + GetSystemMetrics(SM_CYCAPTION);

		RECT rectWindow, rectFull;

		rectWindow.left = (GetSystemMetrics(SM_CXSCREEN) - m_Window.nWidth) / 2;
		rectWindow.top = (GetSystemMetrics(SM_CYSCREEN) - m_Window.nHeight) / 2;
		rectWindow.right = rectWindow.left + m_Window.nWidth;
		rectWindow.bottom = rectWindow.top + m_Window.nHeight;

		rectFull.left = 0;
		rectFull.top = 0;
		rectFull.right = nWidth;
		rectFull.bottom = nHeight;

		if (!m_Window.bWindowed)
		{
			rectWindow.left = rectWindow.top = 0;
			rectWindow.right = nWidth;
			rectWindow.bottom = nHeight;
		}

		if (m_Window.bWindowed)
			SetWindowPos(m_Window.hWnd, 0, rectWindow.left, rectWindow.top, rectWindow.right - rectWindow.left, rectWindow.bottom - rectWindow.top, SW_NORMAL);
		else
			SetWindowPos(m_Window.hWndParent, 0, 0, 0, 0, 0, SW_NORMAL);
	}

	void WindowManager::Render()
	{
		MSG msg;
		for(;;)
		{
			if (!m_Window.hWndParent)
			{
				if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
				{ 
					if (msg.message == WM_QUIT)	break;
					DispatchMessage(&msg);
					continue;
				}
			}

			do { m_dTime = timeGetTime() - m_dTime0; } while (m_dTime < 1);

			if (m_dTime >= m_dFixedDelta)
			{
				m_fDeltaTime = m_dTime / 1000.0f;

				if (m_fDeltaTime > 0.2f)
				{
					m_fDeltaTime = m_dFixedDelta ? m_dFixedDelta / 1000.0f : 0.01f;
				}

				m_fTime += m_fDeltaTime;

				m_dTime0 = timeGetTime();
				if (m_dTime0 - m_dTime0fps <= m_dLockFPS) m_nCurrentFPS++;
				else
				{
					m_nFPS = m_nCurrentFPS;
					m_nCurrentFPS = 0;
					m_dTime0fps = m_dTime0;
				}

				InputManager::GetSingleton().CaptureInput();

				RenderManager::GetSingleton().StartRender();

				GUIManager::GetSingleton().Render();

				m_Window.pRenderProc();

				RenderManager::GetSingleton().EndRender();

				if (m_Window.hWndParent) break;
			}
			else
			{
				if (m_dFixedDelta && m_dTime + 3 < m_dFixedDelta) Sleep(1);
			}
		}
	}
}